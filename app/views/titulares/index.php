<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php";

   ?>
<?php if (count($_SESSION['titulares'])>0): ?>


  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista titulares</h1>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Puesto</th>
          <th>Fecha de nacimiento</th>
          <th>Operaciones</th>

        </tr>
      </thead>
      <tbody>
        <?php foreach ($_SESSION['titulares'] as $jugador): ?>
          <tr>

          <td><?php echo $jugador->id ?></td>
          <td><?php echo $jugador->nombre ?></td>
          <td><?php echo $jugador->findByPosicion($jugador->id_puesto); ?></td>
          <td><?php echo date("d/m/Y", strtotime($jugador->nacimiento)); ?></td>

          <td>
            <a class="btn btn-primary" href="/jugador/quitar/<?php echo $jugador->id ?>">QUITAR</a>
          </td>

          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
    <?php for ($i=1; $i <= $pages; $i++) { ?>
      <?php if ($i != $page): ?>
      <a href="/titular?page=<?php echo $i ?>" class="btn">
        <?php echo $i ?>
      </a>
      <?php else: ?>
        <span class="btn">

        </span>
      <?php endif ?>
    <?php } ?>

    </div>

  </main><!-- /.container -->

<?php else: ?>
<h1>No dispones de titulares</h1>
<?php endif ?>



  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
