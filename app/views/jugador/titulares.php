<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php";
  ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Titulares</h1>

        <ul>
            <li>ID: <?php echo $producttype->id ?></li>
            <li>Nombre: <?php echo $producttype->name ?></li>
        </ul>
    </div>
    <a href="/producttype">Volver</a>
  </main>


  <hr>

  <label>Productos de la categoria;</label>
  <ul>
    <?php foreach ($products as $product): ?>
          <?php if ($product->type_id == $producttype->id): ?>
                <li><?php echo $product->name ?></li>

          <?php endif ?>


    <?php endforeach ?>
</ul>

  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
