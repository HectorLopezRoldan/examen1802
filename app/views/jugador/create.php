<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>
     <?php
          use \App\Models\Posicion;
          require_once '../app/models/Posicion.php';
          $posiciones=Posicion::all();
     ?>
  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Alta de jugador</h1>

      <form method="post" action="/jugador/store">

        <div class="form-group">
          <label>Nombre</label>
          <input type="text" name="nombre" class="form-control">
        </div>
        <div class="form-group">
          <label>Puesto</label>

          <select name="id" class="form-control">

          <?php foreach ($posiciones as $posicion): ?>
                <option value=<?php echo $posicion->id ?>><?php echo $posicion->nombre ?></option>
          <?php endforeach ?>

          </select>



        </div>
        <div class="form-group">
          <label>Nacimiento</label>
          <input type="datetime-local" name="nacimiento" class="form-control">
        </div>

        <button type="submit" class="btn btn-default">Enviar</button>
      </form>
    </div>
  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
