<?php
namespace App\Models;

use PDO;
use Core\Model;

require_once '../core/Model.php';
/**
*
*/
class Posicion extends Model
{

    function __construct()
    {

    }

    public static function all()
    {
        $db = Posicion::db();
        $statement = $db->query('SELECT * FROM puestos');
        $posiciones = $statement->fetchAll(PDO::FETCH_CLASS, Posicion::class);

        return $posiciones;
    }

    public static function paginate($size = 10)
    {
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }

        $offset = ($page - 1) * $size;


        $db = Producttype::db();
        $statement = $db->prepare('SELECT * FROM product_types LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(':pagesize', $size, PDO::PARAM_INT);
        $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
        $statement->execute();

        $products = $statement->fetchAll(PDO::FETCH_CLASS, Producttype::class);

        return $products;
    }

    public static function rowCount()
    {
        $db = Producttype::db();
        $statement = $db->prepare('SELECT count(id) as count FROM product_types');
        $statement->execute();

        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount['count'];
    }




}
