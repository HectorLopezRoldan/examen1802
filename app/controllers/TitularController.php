<?php
namespace App\Controllers;

use \App\Models\Jugador;
require_once '../app/models/Jugador.php';

class TitularController
{

    function __construct()
    {

    }

    public function index()
    {//Mostrar los productos en la cesta
        if(isset($_SESSION['titulares'])){
            $titulares=$_SESSION['titulares'];
        }else{
           $titulares=[];
        }
        $pagesize = 4;
        $rowCount = count($titulares);

        $pages = ceil($rowCount / $pagesize);
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }

        require "../app/views/titulares/index.php";
    }


    public function create()
    {
        require '../app/views/jugador/create.php';
    }

    public function store()
    {
        $jugador = new Jugador();
        $jugador->nombre = $_REQUEST['nombre'];
        $jugador->puesto = $_REQUEST['id'];
        $jugador->nacimiento =$_REQUEST['nacimiento'];
        $jugador->insert();
        header('Location:/jugador');
    }

    public function delete($arguments)
    {
        $id = (int) $arguments[0];
        unset($_SESSIN['titulares'][$id]);

        header('Location:/titulares');
    }



}


