<?php
namespace App\Controllers;

use \App\Models\Jugador;
require_once '../app/models/Jugador.php';

class JugadorController
{

    function __construct()
    {

    }

    public function index()
    {
        $pagesize = 4;
        $jugadores = Jugador::paginate($pagesize);
        $rowCount = Jugador::rowCount();

        $pages = ceil($rowCount / $pagesize);
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }

        require "../app/views/jugador/index.php";
    }


    public function create()
    {
        require '../app/views/jugador/create.php';
    }

    public function store()
    {
        $jugador = new Jugador();
        $jugador->nombre = $_REQUEST['nombre'];
        $jugador->puesto = $_REQUEST['id'];
        $jugador->nacimiento =$_REQUEST['nacimiento'];
        $jugador->insert();
        header('Location:/jugador');
    }

    public function delete($arguments)
    {
        $id = (int) $arguments[0];
        $user = User::find($id);
        $user->delete();
        header('Location:/user');
    }


        public function quitar($arguments)
        {
             $id = (int) $arguments[0];
           unset($_SESSION['titulares'][$id]);
           header('Location:/titular');
        }
        public function titulares($arguments)
        {
        $id = (int) $arguments[0];
        $jugador = Jugador::find($id);

        if(isset($_SESSION['titulares'])){

                if(isset($_SESSION['titulares'][$id])){
                        //ya existe

                }else{


                    $_SESSION['titulares'][$id]=$jugador;
                }

                $_SESSION['msg'] = "exito al añadir el jugador..";

        }else{

            $_SESSION['titulares']=[];

            $_SESSION['titulares'][$id]=$jugador;
        }
        header('Location:/jugador');
        }


}


